import 'package:awesome_notification/controller/notificaton_controller.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Awesome Notification')),
      body: Center(
          child: ElevatedButton(
        onPressed: () {
          NotificationController().callNotification(
            id: 1,
            title: 'Wake up and refresh',
            body:
                "It's time for your morning daily skincare routine! Don't forget to cleanse, tone, and moisturize. ",
            h: 07,
            m: 00,
          );
          NotificationController().callNotification(
            id: 2,
            title: 'Start your day with great skin',
            body:
                "Wake up and small the coffee...and your skincare routine! Start your day off right with Beauty ID's AM skincare routine reminder",
            h: 07,
            m: 10,
          );
          NotificationController().callNotification(
            id: 3,
            title: 'Rise and shine!',
            body:
                "Don't forgot to give your skin some love with Beauty ID's AM skincare routine reminder",
            h: 07,
            m: 20,
          );
          NotificationController().callNotification(
            id: 4,
            title: 'Good morning, beautiful!',
            body:
                "Start your day with the best skincare routine by checking out Beauty ID",
            h: 07,
            m: 30,
          );
          NotificationController().callNotification(
            id: 5,
            title: "Dont't hit snooze on your skincare routine",
            body:
                "Get the best tips and products on your Beauty ID AM skincare routine",
            h: 07,
            m: 40,
          );
          NotificationController().callNotification(
            id: 6,
            title: 'Healthy skin is in!',
            body:
                "Don't forget to drink plenty of water for a hydreated and glowing complexion!",
            h: 12,
            m: 00,
          );
          NotificationController().callNotification(
            id: 7,
            title: 'Sleep tight, wake up bright!',
            body: "Get plenty of rest for a refreshed and revitalize look.",
            h: 21,
            m: 00,
          );
          NotificationController().callNotification(
            id: 8,
            title: 'Goodbye stress, hello beautiful skin!',
            body:
                "Take time to relax and de-stress for a clear and healthy complexion.",
            h: 21,
            m: 00,
          );
          NotificationController().callNotification(
            id: 9,
            title: 'Healthy eating, healthy skin!',
            body:
                "Incorporate fruits and veggies into your diet for a clear and glowing complexion.",
            h: 12,
            m: 00,
          );
          NotificationController().callNotification(
            id: 10,
            title: 'Sweat it out for clear skin!',
            body:
                "Exercise regularly to boost circulation and promote a healthy glow",
            h: 18,
            m: 00,
          );
        },
        child: const Text('Active Notification'),
      )),
    );
  }
}
