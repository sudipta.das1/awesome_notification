import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';

class NotificationController {
  initialize() {
    AwesomeNotifications().initialize(
      null,
      // set the icon to null if you want to use the default app icon
      // 'resource://drawable/res_app_icon',
      [
        NotificationChannel(
            defaultRingtoneType: DefaultRingtoneType.Notification,
            playSound: true,
            channelGroupKey: 'basic_channel_group',
            channelKey: 'basic_channel',
            channelName: 'Basic notifications',
            channelDescription: 'Notification channel for basic tests',
            defaultColor: Color(0xFF9D50DD),
            ledColor: Colors.white)
      ],
      // Channel groups are only visual and are not required
      channelGroups: [
        NotificationChannelGroup(
            channelGroupKey: 'basic_channel_group',
            channelGroupName: 'Basic group')
      ],
      debug: true,
    );
  }

  callNotification({
    required int id,
    required String title,
    required String body,
    required int h,
    required int m,
  }) async {
    await AwesomeNotifications().createNotification(
      content: NotificationContent(
        id: id,
        channelKey: 'basic_channel',
        title: title,
        body: body,
        wakeUpScreen: true,
        category: NotificationCategory.Reminder,
      ),
      schedule: NotificationCalendar.fromDate(
          date: DateTime.parse('2023-03-15 $h:$m:00')),
    );
  }
}
